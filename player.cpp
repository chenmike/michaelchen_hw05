#include "player.h"
#include "location.h"
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
Player::Player(string _name, string _desc, int _size)
	:Character(_name,_desc,_size)
{}

bool Player::act() {
	string in;
	vector<string> exits = cur_loc->getExits();
	cout<<"You are in: "<<cur_loc->getName()<<". Type show to see players, items, and monsters in the room"<<endl;
	getline(cin,in);
	transform(in.begin(), in.end(), in.begin(), ::tolower);
	if((in.find("show"))!=-1) {
		cur_loc->printItems();
	}
	vector<Item*> locitems = cur_loc->getItems();
	cout<<"Type get and an item to add it to your inventory, or type inventory to see what you have, or type quit to end game" << endl;
	getline(cin, in);
	transform(in.begin(), in.end(), in.begin(), ::tolower);
	if((in.find("get"))!=-1) {
		for (int i =0; i<locitems.size();i++) {
			if((in.find(locitems[i]->getName()))!=-1) {
				items.insert(locitems[i]);
				cur_loc->remove(locitems[i]);
			cout<<"You picked up the item"<<endl;
				break;
			}
		}
	}
	if((in.find("quit"))!=-1) {
		return false;
	}
	if((in.find("attack"))!=-1) {
		cout<<"You killed the cyclops"<<endl;
	}
	if((in.find("inventory"))!=-1) {
		set<Item*>::iterator it;	
		for(it=items.begin(); it!=items.end(); ++it) 		{
			cout<<(*it)->getName()<<endl;
		}
	}

cout<<"You are in: "<<cur_loc->getName()<<" Type go to stay or go in a direction to move. Or type quit to end the game."<<endl;
	cur_loc->printExits();
	getline(cin,in);
	transform(in.begin(), in.end(), in.begin(), ::tolower);
	if(in.find("go")!=-1) {
		for(int i=0; i<exits.size(); i++) {
			if(in.find(exits[i])!=-1) {
				moveTo(cur_loc->getLocation(exits[i]));	
				cout<<"You moved to the "<<cur_loc->getName()<<". "<<cur_loc->getDesc()<<endl; 
				return true;
			}
		}	
	}
	if(in.find("quit")!=-1) {
		return false;
	} 
	return true;
}

void Player::addItem(Item* _item) {
	
}
