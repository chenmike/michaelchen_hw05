#ifndef LOCATION_H
#define LOCATION_H

#include <map>
#include <string>
#include <set>
#include <vector>
#include <iterator>
using namespace std;

class Item;

class Location {

	protected:
		string loc;
		string description;
		int size;
		int stuffin;
		set<Item*> items;
		map<string, Location*> exits;
	public:
		Location(string _loc, string _description, int _size);
		string getName();
		string getDesc();
		void link(Location* _loc, string _exit);
		vector<string> getExits();
		bool add(Item* _item); 
		void remove(Item* item);
		void printExits();
		void printItems();
		Location* getLocation(string _dir);
		vector<Item*> getItems();
};

#endif
