#include <iostream>
using namespace std;
#include "location.h"
#include "item.h"
#include "character.h"
#include "cyclops.h"
#include "player.h"
#include <vector>

int main() {
	Location *bathroom = new Location("bathroom", "has a sink", 19);
	Location *ehall = new Location("East Hall", "Long and lonely hallway", 10);
	Location *ent = new Location("Entrance", "An entry way to the abode of evil", 14);
	Location *fount = new Location("Fountain Yard", "The yard with fountain that has crossroads of death and destruction", 15);
	Location *whall = new Location("West Hall", "A hallway that sends shivers down your spine", 13);
	Location *lroom = new Location("Living Room", "All the chairs are covered in soot, spider webs and skulls....", 17);
	Location *cyard = new Location("Open Plain", "The air is cold, the wind howling incessantly", 16);
	Location *mbed = new Location("Master bedroom", "It has a broken bed with moldy sheets", 12);
	Location *gbed = new Location("Guest Bedroom", "Bad smells", 19);
	Location *porch = new Location("Porch", "A place of certain death", 34);
	bathroom->link(lroom, "south");
	bathroom->link(porch,"east");
	ent->link(fount,"north");
	fount->link(ehall,"east");
	fount->link(cyard, "north");
	fount->link(whall,"west");
	ehall->link(gbed,"east");
	ehall->link(mbed,"north");
	ehall->link(fount,"west");
	whall->link(fount,"east");
	whall->link(lroom,"north");
	gbed->link(ehall,"west");
	lroom->link(bathroom, "north");
	lroom->link(cyard,"east");
	cyard->link(lroom,"west");
	cyard->link(porch,"north");
	cyard->link(mbed,"east");
	mbed->link(cyard,"west");
	mbed->link(ehall, "south");
	porch->link(bathroom,"west");
	porch->link(cyard,"south");
	Item* knife = new Item("knife", "A knife on top of skulls", 1);
	fount->add(knife);
	Item* silverspoon = new Item("spoon", "Silver spoon that glows red when held in hand", 1);
	ehall->add(silverspoon);
	Item* sword = new Item("sword","A sword with all mighty power",1);
	gbed->add(sword);
	Item* guide = new Item("guide", "A guide to show you the way to not die",1);
	ent->add(guide); 
	vector<Character*> characters;
	Player* eckroth = new Player("Eckroth", "Computer Science Professor, weak in fighting monsters but extremely intelligent.", 1);
	eckroth->moveTo(ent);
	characters.push_back(eckroth);
	Player* mike = new Player("Mike", "Very brave and courageous but mildly intelligent", 1);
	mike->moveTo(ent);
	characters.push_back(mike);
	Cyclops* cyclops = new Cyclops("Cyclops", "Mythical one-eyed monster", 1);
	cyclops->moveTo(cyard);
	characters.push_back(cyclops);
	bool playing= true;
	while(playing) {
		for(unsigned int i = 0; i<characters.size(); i++) {
			cout<<"The current player is: "<<characters[i]->getName()<<endl;
			if(!characters[i]->act()) {
				playing=false;
				break;
			}
		}	
	}
}
