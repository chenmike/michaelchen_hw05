#ifndef CHARACTER_H
#define CHARACTER_H
#include "item.h"
#include <string>
using namespace std;

class Character : public Item {

	protected:
		Character(string _name, string _desc, int _size);
		bool walk(string _exit);
	public:
		virtual bool act()=0;
};
#endif
