#ifndef CYCLOPS_H
#define CYCLOPS_H
#include "character.h"
#include <string>
using namespace std;

class Cyclops : public Character {

	public:
		Cyclops(string _name, string _description, int _size);
		bool act();
};
#endif
