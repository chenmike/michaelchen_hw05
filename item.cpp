#include "item.h"
#include "location.h"
#include <string>
using namespace std;
Item::Item(string _name, string _description, int _size)
	: name(_name), desc(_description), size(_size), cur_loc(NULL)
{}

bool Item::moveTo(Location *l) {
	
	if(l->add(this)) {
		if(cur_loc != NULL)
			cur_loc->remove(this);

		cur_loc = l;
		return true;	
	}
	return false;

}

string Item::getName() {
	return name;
}

string Item::getDesc() {
	return desc;
}

int Item::getSize() {
	return size;
}
 
