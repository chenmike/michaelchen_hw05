CXX = g++ -g -Wall

all: main

main: main.o player.o cyclops.o location.o item.o character.o
	$(CXX) -o main main.o player.o cyclops.o character.o location.o item.o

main.o: main.cpp location.h player.h cyclops.h item.h character.h
	$(CXX) -c main.cpp

character.o: character.h character.cpp location.h item.h
	$(CXX) -c character.cpp

player.o: player.cpp player.h character.h location.h
	$(CXX) -c player.cpp

cyclops.o: cyclops.cpp cyclops.h character.h location.h
	$(CXX) -c cyclops.cpp

location.o: location.cpp location.h character.h item.h 
	$(CXX) -c location.cpp

item.o: item.cpp item.h
	$(CXX) -c item.cpp item.h

.PHONY: clean
clean:
	rm -f *.o main
