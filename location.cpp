#include "location.h"
#include "item.h"
#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <iostream>
using namespace std;

Location::Location(string _loc, string _description, int _size)
	:	loc(_loc), description(_description), size(_size)
{stuffin=0;}

string Location::getName() {
	return loc;
}
string Location::getDesc() {
	return description;
}
void Location::link(Location* _loc, string _exit) {
	exits[_exit]=_loc;
}
vector<string> Location::getExits() {
	vector<string> exitString;
	map<string, Location* >::iterator it;
	for(it = exits.begin(); it!=exits.end(); ++it) {
		exitString.push_back(it->first);	
	}
	return exitString;		
}
bool Location::add(Item* _item) {
	if(stuffin+_item->getSize()<=size){
		items.insert(_item);
		stuffin+=_item->getSize();
		return true;
	}
	return false;
}
void Location::remove(Item* _item) {
	items.erase(_item);
	stuffin-=_item->getSize();
}
void Location::printExits() {
	cout<<"You may go: "<<endl;
	map<string, Location*>::iterator it;
	for(it = exits.begin(); it != exits.end(); ++it) {
		cout<<it->first<<endl;	
	}
}
void Location::printItems() {
	set<Item* >::iterator it;
	for(it = items.begin(); it != items.end(); ++it) {
		cout<<(*it)->getName()<<endl;	
	}
}

Location* Location::getLocation(string _dir) {
	return exits[_dir];
}

vector<Item*> Location::getItems() {
	set<Item*>::iterator it;
	vector<Item*> myItems;
	for(it = items.begin(); it != items.end(); ++it) {
		myItems.push_back((*it));	
	}
		return myItems;
}


