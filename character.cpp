#include "character.h"
#include <string>
#include "item.h"
#include "location.h"
using namespace std;

Character::Character(string _name, string _description, int _size) : Item(_name, _description, _size)

{}

bool Character::walk(string _exit) {
	moveTo(cur_loc->getLocation(_exit));			
	return true;
}

