#ifndef PLAYER_H
#define PLAYER_H
#include "character.h"
#include <string>
#include <map>
#include <set>
using namespace std;

class Player: public Character {
	private:
		set<Item*> items;
	public:
		Player(string _name, string _desc, int _size);
		bool act();
		void addItem(Item* _item);
		void removeItem(Item* _item);
};
#endif
