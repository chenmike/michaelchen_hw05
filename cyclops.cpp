#include "cyclops.h"
#include "string"
#include <stdlib.h>
#include <vector>
#include "location.h"
#include <iostream>

Cyclops::Cyclops(string _name, string _description, int _size)
	: Character(_name, _description, _size)
{}

bool Cyclops::act() {
	cout<<this->getName()<<" is in: "<< this->cur_loc->getName()<<endl;
	vector<string> dirs=cur_loc->getExits();
	int index = rand()%dirs.size();
	moveTo(cur_loc->getLocation(dirs[index]));
	cout<<this->getName()<<" moved to "<<cur_loc->getName()<<endl;
	return true;
}
