#ifndef ITEM_H
#define ITEM_H

#include <string>
using namespace std;
class Location;

class Item {
	public:
		Item(string name, string desc, int size);
		bool moveTo(Location* loc);
		string getName();
		string getDesc();
		int getSize();
	protected:
		string name;
		string desc;
		int size;
		Location *cur_loc;
};
#endif
